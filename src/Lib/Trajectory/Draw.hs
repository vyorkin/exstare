module Lib.Trajectory.Draw where

import Control.Monad (guard)
import Linear (V2(..), norm)
import Linear.Affine (qdA)

import qualified Data.Vector.Storable as Vector
import qualified Graphics.Rendering.OpenGL as GL

import Components

import qualified Lib
import qualified Lib.Window as Window
import qualified Lib.Trajectory as Trajectory
import qualified Programs.Solid as Solid

drawSegments
  :: ( Lib.ToGL color
     , Lib.GLType color ~ GL.Vector4 Float
     )
  => Int -> color -> [Trajectory.Segment] -> SystemW ()
drawSegments 0 _color _segments = pure ()
drawSegments limit color segments =
  Solid.drawWith (Lib.toGL color) $ \coord2d -> do
    onCamera <- Window.onCamera minDist
    Lib.drawQuads coord2d . Vector.concat $ do
      (curPos, nextPos) <- path
      let
        V2 px py = Trajectory.sPos curPos

        deltaPos = Trajectory.sPos curPos - Trajectory.sPos nextPos
        deltaLen = norm deltaPos

      guard $ onCamera px py

      pure $ Lib.quadTurn (deltaLen * 0.5) 2 px py (Lib.unturn deltaPos)
  where
    minDist = 8
    minDist2 = minDist * minDist

    reduced = take limit $ Trajectory.reducedPath minDist segments

    path =
      case zip reduced (drop 1 reduced) of
        (Trajectory.Segment{sPos}, _) : rest ->
          takeWhile (notClose sPos . snd) rest
        rest ->
          rest

    notClose startPos Trajectory.Segment{sPos} = qdA sPos startPos > minDist2
