module Lib.Render where

import Control.Monad.IO.Class (MonadIO(..))
import Data.StateVar (($=))
import Foreign.Ptr (nullPtr)

import qualified Graphics.Rendering.OpenGL as GL

toTexture :: MonadIO m => (GL.FramebufferObject -> GL.TextureObject -> m ()) -> m ()
toTexture action = do
  (buf, tex) <- liftIO $ do
    [buf] <- GL.genObjectNames 1
    GL.bindFramebuffer GL.DrawFramebuffer $= buf

    [tex] <- GL.genObjectNames 1
    let target = GL.Texture2D
    GL.textureBinding target $= Just tex

    GL.texImage2D
      GL.Texture2D
      GL.NoProxy
      0
      GL.RGBA8
      (GL.TextureSize2D 1024 768)
      0
      (GL.PixelData GL.ABGR GL.UnsignedByte nullPtr)

    GL.textureFilter target $= ((GL.Nearest, Nothing), GL.Nearest)
    -- GL.textureWrapMode target GL.S $= (GL.Repeated, GL.Repeat) -- XXX: clamp?
    -- GL.textureWrapMode target GL.T $= (GL.Repeated, GL.Repeat)

    GL.textureBinding target $= Nothing

    GL.framebufferTexture2D GL.DrawFramebuffer (GL.ColorAttachment 0) target tex 0
    GL.framebufferStatus GL.DrawFramebuffer >>= \case
      GL.Complete ->
        -- Lib.debugM $ "Framebuffer bound to texture: " <> show (buf, tex)
        pure ()
      err ->
        error $ "Framebuffer made a boo-boo: " <> show err

    pure (buf, tex)

  action buf tex

  liftIO $ do
    GL.deleteObjectNames [buf]
    GL.deleteObjectNames [tex]
