module Components.Platform.Draw where

import Apecs (get, global)
import Control.Lens ((&), (.~))
import Linear (V2(..), V4(..), unangle, (^*))
import System.FilePath ((</>))

import qualified Apecs
import qualified Graphics.Rendering.OpenGL as GL

import Components
import Components.Platform.Types (Platform(..))
import Lib.Trajectory.Draw (drawSegments)

import qualified Components.Missile.System as Missile
import qualified Components.Missile.Trajectory as Missile
import qualified Components.Motion.Types as Motion
import qualified Components.Textures as Textures
import qualified Programs.Sprite as Sprite

draw :: SystemW ()
draw =
  Apecs.cmapM_ $ \(Platform{..}, Position pos, Velocity vel, Motion.Trajectory{..}) -> do
    Apecs.cmapM_ $ \Cursor{_cursorCamera=target} ->
      case _platformWeapon of
        Missile.Slug -> do
          drawSegments 100 (GL.Vector4 0 0.75 1 0.75) $
            Missile.projectSlug (Position pos) (Velocity vel) target

        Missile.Rocket -> do
          drawSegments 100 (GL.Vector4 0 1 0.5 0.75) $
            Missile.projectRocketShot (Position pos) (Velocity vel) target

    let
      turn = unangle vel / pi / 2
      eff = mempty
        & Sprite.effectsTurn .~ turn
        -- & Sprite.effectsOutline .~ Just (V4 0 0.5 1 4)

    -- XXX: locked until post-LD release
    -- Apecs.get Apecs.global >>= \case
    --   Nothing ->
    --     pure ()
    --   Just (Camera{_cameraScale}, Cursor{_cursorCamera}) -> do
    --     let distance = max _platformSize . min 300 $ distanceA pos _cursorCamera
    --     let limited = pos + normalize (_cursorCamera - pos) ^* min 300 distance
    --     let color = 0.0
    --     Solid.drawLine pos limited color (4 * _cameraScale)

    drawSegments 500 (V4 0 1 0 0.5) _trajectorySegments

    Sprite.draw eff (Textures.Key "platform") (V2 _platformSize _platformSize) pos

drawUI :: SystemW ()
drawUI = do
  Window{..} <- Apecs.get Apecs.global

  Apecs.cmapM_ $ \(Platform{..}, Position _pos, Velocity _vel) -> do
    let
      slug =
        if _platformWeapon == Missile.Slug then
          mempty & Sprite.effectsOutline .~ Just 4
        else
          mempty
    Sprite.draw slug (Textures.Key $ "missile" </> "slug") 64
      (V2 (-100) (-100) + V2 _windowWidth _windowHeight ^* 0.5)

    let
      rocket =
        if _platformWeapon == Missile.Rocket then
          mempty & Sprite.effectsOutline .~ Just 4
        else
          mempty
    Sprite.draw rocket (Textures.Key $ "missile" </> "rocket") 64
      (V2 (-100) (-200) + V2 _windowWidth _windowHeight ^* 0.5)
