module Components.Missile.Draw where

import Control.Lens ((&), (.~))
import Control.Monad (when)
import Linear (V2(..))
import System.FilePath ((</>))

import qualified Apecs
import qualified Graphics.Rendering.OpenGL as GL

import Components
import Components.Missile.Types (Missile(..), MissileType(..))
import Components.Motion.Types (Trajectory(..))
import Lib.Trajectory.Draw (drawSegments)

import qualified Components.Textures as Textures
import qualified Lib
import qualified Lib.Window as Window
import qualified Programs.Sprite as Sprite

draw :: SystemW ()
draw = do
  Camera{..} <- Apecs.get Apecs.global
  onCamera <- Window.onCamera 32

  Apecs.cmapM_ $ \(Missile{..}, Position pos, Velocity vel, Trajectory{_trajectorySegments}) -> do
    let
      (key, size, fixup) = case _missileType of
        Slug   -> (Textures.Key $ "missile" </> "slug",   16,          5/8)
        Rocket -> (Textures.Key $ "missile" </> "rocket", 32, negate $ 1/8)

      decayStart = _missileFuse * 0.9
      decay =
        if _missileTimer < decayStart then
          0
        else
          (_missileTimer - decayStart) / (_missileFuse - decayStart)

      eff = mempty
        & Sprite.effectsTurn .~ (Lib.unturn vel + fixup)
        & Sprite.effectsOpacity .~ 1 - decay

    case _missileType of
      Slug ->
        pure ()
      Rocket -> do
        let len = truncate $ 100 * (1 - decay)
        drawSegments len (GL.Vector4 1 1 0 0.5) _trajectorySegments

    let V2 px py = pos
    when (onCamera px py) $
      Sprite.draw eff key size pos
