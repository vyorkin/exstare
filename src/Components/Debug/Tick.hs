module Components.Debug.Tick where

import Apecs (Entity, Not(..), cmap, newEntity)
import Linear (V3)

import Components (Turn, Position(..), SystemW)
import Components.Debug.Types

mark :: Position -> Turn -> Float -> V3 Float -> Float -> SystemW Entity
mark pos turn timer color size = newEntity
  ( Foo
      { _fooColor = color
      , _fooTimer = timer
      , _fooSize  = size
      }
  , pos
  , turn
  )

tick :: Float -> SystemW ()
tick dt =
  cmap $ \Foo{..}  ->
    let
      timer' = _fooTimer - dt
    in
      if timer' <= 0 then
        Left $ Not @(Foo, Position, Turn)
      else
        Right Foo
          { _fooTimer = timer'
          , ..
          }
