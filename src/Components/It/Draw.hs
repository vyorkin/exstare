module Components.It.Draw where

import Control.Lens ((&), (.~))
import Linear (V3(..), V4(..), lerp, (^*))

import qualified Apecs

import Components
import Components.It.Types (It(..))

import qualified Components.Textures as Textures
import qualified Programs.Ring as Ring
import qualified Programs.Sprite as Sprite

draw :: SystemW ()
draw =
  Apecs.cmapM_ $ \(It{..}, Orbital{..}, Position pos) -> do
    let size = 2.0 ^* _orbitalRadius

    let
      blueish = V4 0 0.5 1 1
      reddish = V4 1 0.5 0 1

      color = lerp (min 1 _itDevastation) reddish blueish

      gammaRed = 0.125
      gammaGreen = 0.1
      gammaBlue = 0.1
      devGamma = V3
        ((1 + gammaRed) ** min 8 _itDevastation - gammaRed)
        (gammaGreen + (1 - gammaGreen) ** (1 + min 8 _itDevastation))
        (gammaBlue + (1 - gammaBlue) ** (1 + min 8 _itDevastation))

      eff = mempty
        & Sprite.effectsGamma .~ devGamma

    Ring.draw size 0 0 (0.0, 0.9, 0.1) color
    Sprite.draw eff (Textures.Key "planet") size pos

drawUI :: SystemW ()
drawUI =
  -- TODO: score readout
  pure ()
