uniform vec2  u_size;
uniform float u_radius;
uniform float u_inner;
uniform float u_outer;
uniform vec4  u_color;

varying vec2 f_texcoord;

void main() {
  // Screen-space:
  // vec2 pos = gl_FragCoord.xy / u_resolution;

  // Texure-space:
  // vec2 pos = f_texcoord;

  // Texure-space, squared:
  vec2 pos2 = f_texcoord;
  if (u_size.x > u_size.y) {
    pos2.y /= u_size.y / u_size.x;
    pos2.y -= 0.25;
  } else
  if (u_size.y > u_size.x) {
    pos2.x /= u_size.x / u_size.y;
    pos2.x -= 0.25;
  }

  // Polar from square-space:
  vec2 posP = vec2(0.5 - pos2);
  float r = length(posP) * 2.0;
  // float a = atan(posP.y, posP.x); // + u_time;

  float f = u_radius;
  float s = smoothstep(f - u_inner, f, r) - smoothstep(f, f + u_outer, r);

  gl_FragColor = vec4(u_color.xyz, u_color.w * s);
}
