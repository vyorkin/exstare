module Main where

import Control.Monad (when)
import Data.StateVar (($=))
import System.Environment (getArgs)

import qualified Apecs
import qualified Apecs.System.Random as Random
import qualified Graphics.Rendering.OpenGL as GL
import qualified SDL

import Components

import qualified Components.Programs as Programs
import qualified Components.Textures as Textures
import qualified Components.Platform.System as Platform
import qualified Lib
import qualified Lib.Window as Window
import qualified MainLoop
import qualified Scene

main :: IO ()
main = do
  args <- getArgs
  let
    framesLimit =
      case args of
        "--timedemo" : frames : _rest ->
          read frames
        _ ->
          -1

  SDL.initializeAll

  SDL.HintRenderScaleQuality $= SDL.ScaleNearest -- Linear
  window <- SDL.createWindow "Exstare" $
    glWindow $
      if framesLimit > 0 then
        SDL.FullscreenDesktop
      else
        SDL.Maximized
  SDL.showWindow window

  _glContext <- SDL.glCreateContext window

  SDL.swapInterval $=
    if framesLimit > 0 then
      SDL.ImmediateUpdates
    else
      SDL.SynchronizedUpdates

  GL.blend $= GL.Enabled
  GL.blendFunc $= (GL.SrcAlpha, GL.OneMinusSrcAlpha)

  world <- Components.initWorld
  (frames, framesTime) <- Apecs.runWith world $ do
    windowSize <- SDL.glGetDrawableSize window
    Lib.debugM $ "Initial window: " <> show windowSize
    Window.setSize windowSize

    Textures.loadAll
    Programs.loadAll

    if framesLimit > 0 then
      Platform.shoot360
    else
      Random.initialize

    Scene.init

    initialSeconds <- Lib.seconds
    lastFrame <- MainLoop.go window framesLimit initialSeconds
    finalSeconds <- Lib.seconds

    pure
      ( if framesLimit > 0 then
          framesLimit
        else
          lastFrame
      , finalSeconds - initialSeconds
      )

  let
    message = unwords
      [ "Presented"
      , show frames
      , "frames in"
      , show framesTime
      , "seconds."
      , "Mean FPS:"
      , show $ fromIntegral frames / framesTime
      ]
  Lib.debugM message
  when (framesLimit > 0) $
    writeFile "timerun.txt" message

  SDL.destroyWindow window
  SDL.quit

glWindow :: SDL.WindowMode -> SDL.WindowConfig
glWindow mode = SDL.defaultWindow
  { SDL.windowGraphicsContext = SDL.OpenGLContext profile
  , SDL.windowResizable       = True
  , SDL.windowMode            = mode
  }
  where
    -- TODO: find out that "Core" deprecates and disable compat
    profile = SDL.defaultOpenGL
      { SDL.glProfile =
          -- SDL.Core SDL.Normal 4 5
          SDL.Compatibility SDL.Normal 4 5
      }
