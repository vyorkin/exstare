{-# LANGUAGE TemplateHaskell #-}

module Components.Missile.Types where

import Apecs.TH (makeMapComponents)
import Control.Lens.TH (makeLenses)

data Missile = Missile
  { _missileTimer :: Float
  , _missileFuse  :: Float
  , _missileType  :: MissileType
  }
  deriving (Show)

data MissileType
  = Slug
  | Rocket -- TODO: move fuse to engine-enabled missiles
  deriving (Eq, Ord, Show, Bounded, Enum)

makeLenses ''Missile

makeMapComponents
  [ ''Missile
  ]
