{-# LANGUAGE TemplateHaskell #-}

module Components
  ( World
  , SystemW
  , initWorld

  , Camera(..)
  , cameraOffset
  , cameraScale
  , Cursor(..)
  , DragStart(..)
  , Window(..)
  , windowWidth
  , windowHeight
  , windowScale

  , Position(..)
  , Mass(..)
  , Velocity(..)
  , Impulse
  , Turn(..)
  , Spin(..)
  , Momentum
  , Orbital(..)
  ) where

import Apecs
import Apecs.Components.Random (RandomGen)
import Apecs.TH (makeMapComponents)
import Control.Lens.TH (makeLenses)
import Linear (V2)

import Components.Comet.Types (Comet)
import Components.It.Types (It)
import Components.Missile.Types (Missile)
import Components.Platform.Types (Platform)
import Components.Programs (Programs)
import Components.Textures (Textures)

import qualified Components.Debug.Types as Debug
import qualified Components.Explosion.Types as Explosion
import qualified Components.Motion.Types as Motion
import qualified Components.Time.Types as Time

newtype Position = Position
  { unPosition :: V2 Float
  }
  deriving (Eq, Ord, Show)

newtype Mass = Mass
  { unMass :: Float
  }
  deriving (Eq, Ord, Show)

newtype Velocity = Velocity
  { unVelocity :: V2 Float
  }
  deriving (Eq, Ord, Show)

type Impulse = (Mass, Velocity)

newtype Turn = Turn
  { unTurn :: Float
  }
  deriving (Eq, Ord, Show)

newtype Spin = Spin
  { unSpin :: Float
  }
  deriving (Eq, Ord, Show)

type Momentum = (Mass, Spin)

data Orbital = Orbital
  { _orbitalRadius :: Float
  }
  deriving (Eq, Ord, Show)

data Camera = Camera
  { _cameraScale :: Float
  , _cameraOffset :: V2 Float
  }
  deriving (Eq, Show)

instance Semigroup Camera where
  a <> b = Camera
    { _cameraScale  = _cameraScale a * _cameraScale b
    , _cameraOffset = _cameraOffset a + _cameraOffset b
    }

instance Monoid Camera where
  mempty = Camera
    { _cameraScale  = 1.0
    , _cameraOffset = 0
    }

instance Component Camera where
  type Storage Camera = Global Camera

-- TODO: index positions with coordinate space
data Cursor = Cursor
  { _cursorWindow :: V2 Float
  , _cursorCamera :: V2 Float
  }
  deriving (Show)

instance Component Cursor where
  type Storage Cursor = Unique Cursor

data DragStart = DragStart
  { _dragStartWindow :: V2 Float
  , _dragStartCamera :: V2 Float
  }
  deriving (Show)

instance Component DragStart where
  type Storage DragStart = Unique DragStart

data Window = Window
  { _windowWidth  :: Float
  , _windowHeight :: Float
  , _windowScale  :: Float
  }
  deriving (Eq, Show)

instance Component Window where
  -- XXX: may be not yet initialized or otherwise unavailable
  type Storage Window = Unique Window

makeMapComponents
  [ ''Position
  , ''Mass
  , ''Velocity
  , ''Turn
  , ''Spin
  , ''Orbital
  ]

makeWorld "World"
  [ ''Textures
  , ''Programs

  , ''Camera
  , ''Cursor
  , ''DragStart
  , ''Window

  , ''Position
  , ''Mass
  , ''Velocity
  , ''Turn
  , ''Spin

  , ''Orbital

  , ''It
  , ''Platform
  , ''Comet
  , ''Missile

  , ''Debug.Foo
  , ''Explosion.Explosion
  , ''Motion.Trajectory
  , ''Time.Pause
  , ''RandomGen
  ]

type SystemW a = SystemT World IO a

makeLenses ''Camera
makeLenses ''Window
