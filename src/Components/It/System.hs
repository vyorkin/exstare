module Components.It.System where

import Apecs (Entity, Not(..), cmap, cmapM_, newEntity, ($=))

import Components
import Components.It.Types

import qualified Config
import qualified Lib

type ItComponents = (It, Orbital, Position, Mass)

new :: SystemW Entity
new = newEntity
  ( It
      { _itTimer       = 0
      , _itDevastation = 0
      }
  , Position 0
  , Mass Config.itMass
  , Orbital
      { _orbitalRadius = Config.itRadius
      }
  )

destroy :: Entity -> SystemW ()
destroy e = e $= Not @ItComponents

tick :: Float -> SystemW ()
tick dt = do
  cmap $ \It{..} -> It
    { _itTimer       = _itTimer + dt
    , _itDevastation = max 0.0 $ _itDevastation - Config.devastationDecay * dt
    }
  Lib.sometimes (dt / 10) $ cmapM_ $ \it ->
    Lib.debugM $ show @It it
