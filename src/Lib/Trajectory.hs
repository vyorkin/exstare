-- | Thanks, SpaceMar!

module Lib.Trajectory
  ( Segment(..)
  , Force(..)
  , project
  , project_
  , Ann(..)
  , inspect
  , Inspection(..)
  , pull
  , pullV

  , reducedPath
  ) where

import Control.Monad (guard)
import Data.List (unfoldr)
import Linear.Affine (qdA)
import Linear.Metric (norm, normalize)
import Linear.V2 (V2(..))
import Linear.Vector ((^*))

import qualified Config

data Segment = Segment
  { sIx     :: Int      -- Step index
  , sPos    :: V2 Float -- Step position
  , sVel    :: V2 Float -- Projected velocity vector
  , sNorm   :: Float    -- Scalar velocity
  , sDecr   :: Bool     -- Scalar velocity decreased?
  , sForce  :: Force
  , sAnn    :: Ann
  }
  deriving (Eq, Ord, Show)

data Ann
  = Fall      -- ^ Unpowered orbit
  | Impact    -- ^ Powered collision
  | Apoapsis  -- ^ Local minimum
  | Periapsis -- ^ Local maximum
  deriving (Eq, Ord, Show)

data Force
  = Passive
  | Constant Float Float
  deriving (Eq, Ord, Show)

project_ :: Float -> (V2 Float, V2 Float) -> Force -> [Segment]
project_ force = project defaultDt force defaultBodies
  where
    defaultDt = Config.dt
    defaultBodies = [(Config.itMass, V2 0 0)]

project :: Float -> Float -> [(Float, V2 Float)] -> (V2 Float, V2 Float) -> Force -> [Segment]
project dt maxForce gs (startPos, startVel) extraForce = unfoldr_ nextSegment initialSegment
  where
    -- XXX: massage step function into being both accumulator and result.
    unfoldr_ f = unfoldr $ fmap (\a -> (a, a)) . f

    initialSegment = Segment
      { sIx     = 0
      , sPos    = startPos
      , sVel    = startVel
      , sNorm   = norm startVel
      , sDecr   = False
      , sAnn    = Fall
      , sForce  = extraForce
      }

    nextSegment Segment{..} =
      if sAnn == Impact then
        Nothing
      else
        case step sPos (sVel + eVel) of
          Nothing ->
            Just Segment
              { sIx  = succ sIx
              , sAnn = Impact
              , ..
              }
          Just vel' ->
            Just Segment
              { sIx     = succ sIx
              , sAnn    = ann'
              , sPos    = pos'
              , sVel    = vel'
              , sNorm   = vel_
              , sDecr   = decr'
              , sForce  = force'
              }
            where
              pos' = sPos + vel' ^* dt
              vel_ = norm vel'
              decr' = vel_ < sNorm
              ann' =
                case (sDecr, decr') of
                  _ | sIx < 2 ->
                    Fall
                  (True, False) ->
                    Apoapsis
                  (False, True) ->
                    Periapsis
                  _ ->
                    Fall
        where
          (force', eVel) = case sForce of
            Passive ->
              (Passive, 0)
            Constant acc timer
              | timer <= 0 ->
                  ( Passive
                  , 0
                  )
              | otherwise ->
                  ( Constant acc $ timer - dt
                  , normalize sVel ^* (acc * dt)
                  )

    step massPos intialMoment = foldr stepF (pure intialMoment) gs
      where
        stepF (gravMass, gravPos) accM = do
          let
            force = pull gravMass gravPos massPos
            forceV = normalize (gravPos - massPos) ^* force
          guard $ force < maxForce
          acc <- accM
          pure $ acc + forceV

pullV :: Float -> V2 Float -> V2 Float -> V2 Float
pullV gm gp mp =
  normalize (gp - mp) ^* pull gm gp mp
{-# INLINE pullV #-}

pull :: Float -> V2 Float -> V2 Float -> Float
pull gm gp mp =
  gm / qdA gp mp
{-# INLINE pull #-}

data Inspection = Inspection
  { apsides :: [Segment]
  , entry   :: Maybe Segment
  }

inspect :: [Segment] -> Inspection
inspect segments = Inspection{..}
  where
    apsides = do
      seg@Segment{sAnn} <- segments
      guard (sAnn == Apoapsis || sAnn == Periapsis)
      pure seg

    entry = case dropWhile notLanding segments of
      seg@Segment{sAnn=Impact} : _rest ->
        Just seg
      _ ->
        Nothing

    notLanding Segment{sAnn} = sAnn /= Impact

reducedPath :: Float -> [Segment] -> [Segment]
reducedPath _minDelta []                = []
reducedPath _minDelta [single]          = [single]
reducedPath minDelta (start : segments) = start : go 0 segments
  where
    go dist = \case
      []           -> []
      [single]     -> [single]
      [a, b]       -> [a, b]
      a : b : rest ->
        let
          deltaDist = dist + sNorm b * Config.dt
        in
          if deltaDist < minDelta then
            go deltaDist (a : rest)
          else
            a : go 0 (b : rest)
