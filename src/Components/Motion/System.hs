module Components.Motion.System where

import Apecs (Not(..), cfold, cmap, cmapM)
import Data.Foldable (foldl')
import Linear (V2(..), normalize, (^*))
import Linear.Affine (qdA)

import Components

import qualified Components.Motion.Types as Motion
import qualified Config
import qualified Lib
import qualified Lib.Trajectory as Trajectory

tick :: Float -> SystemW ()
tick dt = do
  cmapM $ \(Mass _mass, Velocity initialVel, Position pos, _ :: Not Orbital, trajectory) ->
    case trajectory of
      Nothing -> do
        Velocity nextVel <- stepPassive pos initialVel
        pure $ Left
          ( Velocity nextVel
          , Position $ pos + nextVel ^* dt
          )

      Just Motion.Trajectory{_trajectorySegments} ->
        case _trajectorySegments of
          Trajectory.Segment{..} : next -> do
            Lib.debugM $ "some trajectory"
            pure $ Right
              ( Motion.Trajectory next
              , Velocity sVel
              , Position sPos
              )
          [] ->
            error "empty trajectory set in motion, should've been intercepted in previous frame."

  cmap $ \(Spin spin, Turn turn) ->
    Turn $ turn + spin * dt

getOrbitals :: SystemW [(Float, V2 Float)]
getOrbitals =
  flip cfold mempty $ \acc (Orbital{}, Mass mass, Position pos) ->
    (mass, pos) : acc

projectPassive :: Position -> Velocity -> SystemW Motion.Trajectory
projectPassive (Position pos) (Velocity initialVel) = do
  bodies <- getOrbitals
  pure . Motion.Trajectory $
    Trajectory.project
      Config.dt
      1.0
      bodies
      (pos, initialVel)
      Trajectory.Passive

projectForce :: Position -> Velocity -> Trajectory.Force -> SystemW Motion.Trajectory
projectForce (Position pos) (Velocity initialVel) force = do
  bodies <- getOrbitals
  pure . Motion.Trajectory $
    Trajectory.project
      Config.dt
      1.0
      bodies
      (pos, initialVel)
      force

stepPassive :: V2 Float -> V2 Float -> SystemW Velocity
stepPassive pos initialVel = do
  bodies <- getOrbitals
  pure . Velocity $ foldl' pulls initialVel bodies
  where
    pulls vel (bodyMass, bodyPos) = vel + pullV bodyMass bodyPos pos

pullV :: Float -> V2 Float -> V2 Float -> V2 Float
pullV gm gp mp =
  normalize (gp - mp) ^* pull gm gp mp
{-# INLINE pullV #-}

pull :: Float -> V2 Float -> V2 Float -> Float
pull gm gp mp =
  gm / qdA gp mp
{-# INLINE pull #-}
