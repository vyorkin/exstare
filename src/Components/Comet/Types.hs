{-# LANGUAGE TemplateHaskell #-}

module Components.Comet.Types where

import Apecs.TH (makeMapComponents)
import Control.Lens.TH (makeLenses)

data Comet = Comet
  { _cometSighted :: Float
  , _cometSize    :: Float
  , _cometTimer   :: Float
  , _cometType    :: CometType
  } deriving (Show)

data CometType
  = Rock
  | Spiked
  deriving (Eq, Ord, Show, Enum, Bounded)

makeLenses ''Comet

makeMapComponents
  [ ''Comet
  ]
