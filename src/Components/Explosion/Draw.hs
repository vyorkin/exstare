module Components.Explosion.Draw where

import Control.Monad (guard)
import Linear (lerp)

import qualified Apecs

import Components
import Components.Explosion.Types

import qualified Programs.Ring as Ring

drawForeground :: SystemW ()
drawForeground = draw False

drawBackground :: SystemW ()
drawBackground = draw True

draw :: Bool -> SystemW ()
draw bg =
  Apecs.cmapM_ $ \(Explosion{..}, pos, turn) -> do
    case _explosionType of
      Rings rings ->
        mapM_ (drawRing pos turn) $ do
          ring@Ring{_ringBackground} <- rings
          guard $ _ringBackground == bg
          pure ring

drawRing :: Position -> Turn -> Ring -> SystemW ()
drawRing (Position origin) (Turn turn) Ring{..} =
  Ring.draw _ringSize origin turn params color
  where
    params =
      ( _ringInner  * scale
      , _ringRadius * scale
      , _ringOuter  * scale
      )

    color =
      if _ringSpeed < 0 then
        lerp scale 1.0 _ringColor
      else
        lerp scale 0 $ lerp scale _ringColor 1.0

    scale =
      if _ringSpeed < 0 then
        1 - _ringTimer / _ringTimeLimit
      else
        _ringTimer / _ringTimeLimit
