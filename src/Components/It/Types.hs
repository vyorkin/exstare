{-# LANGUAGE TemplateHaskell #-}

module Components.It.Types where

import Apecs.TH (makeMapComponents)
import Control.Lens.TH (makeLenses)

data It = It
  { _itTimer       :: Float
  , _itDevastation :: Float
  }
  deriving (Show)

makeLenses ''It

makeMapComponents
  [ ''It
  ]
