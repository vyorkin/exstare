module Draw where

import Linear (V4(..), (^*))
import System.FilePath ((</>))

import qualified Apecs

import Components (SystemW)

import qualified Components.Comet.Draw as Comet
import qualified Components.Debug.Draw as Debug
import qualified Components.Explosion.Draw as Explosion
import qualified Components.It.Draw as It
import qualified Components.Missile.Draw as Missile
import qualified Components.Platform.Draw as Platform
import qualified Components.Textures as Textures
import qualified Lib
import qualified Lib.Window as Window
import qualified Programs.Textured as Textured

drawBackdrop :: SystemW ()
drawBackdrop = do
  window <- Apecs.get Apecs.global
  let side = Window.sizeMax window
  Textured.drawWith (Textures.Key $ "bg" </> "stars") $ \coord2d -> do
    Lib.drawQuads coord2d $
      Lib.toQuad $ V4 1 1 0 0 ^* side

drawScene :: SystemW ()
drawScene = do
  Explosion.drawBackground
  It.draw
  Debug.draw
  Missile.draw
  Platform.draw
  Comet.draw
  Explosion.drawForeground

drawUI :: SystemW ()
drawUI = do
  It.drawUI
  Platform.drawUI
