module Components.Platform.System where

import Apecs (Entity, Not(..), cmap, cmapM_, get, global, newEntity, ($=))
import Data.Foldable (for_)
import Control.Lens ((.~), (%~), (+~))
import Control.Monad (void)
import Linear ((^*))
import Linear (angle)

import Components
import Components.Platform.Types

import qualified Components.Missile.System as Missile
import qualified Components.Motion.System as Motion
import qualified Components.Motion.Types as Motion

type PlatformComponents =
  ( Platform
  , Mass
  , Position, Velocity
  -- XXX: Spin, Turn not used, the platform is self-orienting
  , Motion.Trajectory
  )

new :: Float -> Position -> Velocity -> SystemW Entity
new size pos vel = do
  trajectory <- Motion.projectPassive pos vel
  newEntity
    ( Platform
        { _platformSize   = size
        , _platformTimer  = 0
        , _platformWeapon = Missile.Slug
        }
    , pos
    , vel
    , Mass size
    , trajectory
    )

destroy :: Entity -> SystemW ()
destroy e = e $= Not @PlatformComponents

tick :: Float -> SystemW ()
tick dt = do
  cmap $ platformTimer +~ dt
  -- cmapM_ $ \Cursor{} ->
  --   Lib.sometimes (10 * dt) shoot

selectWeapon :: Int -> SystemW ()
selectWeapon = \case
  1 ->
    cmap $ platformWeapon .~ Missile.Slug
  2 ->
    cmap $ platformWeapon .~ Missile.Rocket
  _ ->
    pure ()

nextWeapon :: SystemW ()
nextWeapon = cmap $
  platformWeapon %~ \current ->
    if current == maxBound then
      minBound
    else
      succ current

shoot :: SystemW ()
shoot = do
  Cursor{_cursorCamera=target} <- get global
  cmapM_ $ \(Platform{..}, Position source, Velocity sourceVel) ->
    void $
      case _platformWeapon of
        Missile.Slug ->
          Missile.shootSlug (Position source) (Velocity sourceVel) target
        Missile.Rocket ->
          Missile.shootRocket (Position source) (Velocity sourceVel) target

shoot360 :: SystemW ()
shoot360 = do
  -- cmapM_ $ \(Platform{..}, Position source, Velocity sourceVel) ->
    for_ [1..salvo] $ \deg -> do
      let vec = angle (360 / salvo * pi * deg / 180)
      let src = vec ^* 300
      let tgt = vec ^* 350
      void $ Missile.shootRocket (Position src) (Velocity 0) tgt
  where
    salvo = 36

altPrepare :: SystemW ()
altPrepare =
  -- TODO
  pure ()

altShoot :: SystemW ()
altShoot =
  -- TODO
  pure ()
